# RconChat

RconChat is a Factorio mod that presents an RCON interface for sending and getting chat messages.

Initially designed for the FishBus servers as part of the scenario.  Extracted it's functionality into a mod (and added some new routines) to turn it into its own mod.  With an external process, this means we can get our normal chat interactions with a discord bot to create a chat bridge between our discord channel and the game server.

### Usage

While this mod is pretty add and forget, the rest of the setup is not so simple.  Somewhere, a program such as https://gitlab.com/FishBus/chat-discord must be running to communicate with this mod and make it do something interesting.
